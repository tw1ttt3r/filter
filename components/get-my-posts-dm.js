import {LitElement} from 'lit-element';

export default class GetMyPostsDm extends LitElement {
  static get properties(){
    return{

    }
  }

  constructor(){
    super();
  }
  _handleSuccessResponse(data){
    try{
      let users = this._formatData(data);
      this.dispatchEvent(new CustomEvent('success-call', {detail:users}));
    } catch(e){
      console.log(e);
      this._handleErrorResponse(data);
    }
  }

  _formatData(users){
    let formatPosts = []
    users.forEach((user) =>{
      formatPosts.push({
        'name':user.data.name,
        'age':user.data.age
      })
    });
    return formatPosts;
  }

  _handleErrorResponse(dataErr){
    let erroConfig = {
      title: 'Error de conexion',
      body: 'Intentan de nuevio mas tarde'
    }
    this.dispatchEvent(new CustomEvent('error-call', {detail:erroConfig}));
  }

  generateRequest(){
    fetch('./components/sources/posts.json')
    .then(response => response.json())
    .then((response)=>{
      this._handleSuccessResponse(response);
    })
    .catch((err)=>{
      this._handleErrorResponse(err);
    })
  }
}

customElements.define('get-my-posts', GetMyPostsDm);




